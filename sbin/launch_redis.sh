#!/usr/bin/env bash

echo "you should launch redis as root, using the \`redis-server --bind \$(hostname -I) --protected-mode no\` command"
